import boto3
S3_client = boto3.client("s3")
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('rest')
def lambda_handler(event, context):
    s3_bucketname = event['Records'][0]['s3']['bucket']['name']
    s3_key = event['Records'][0]['s3']['object']['key']
    response = S3_client.get_object(Bucket=s3_bucketname,Key=s3_key)
    data = response['Body'].read().decode('utf-8')
    # print(data,type(data))
    res = data.split()
    # print(res)
    for i in res:
        # print(i)
        re_data = i.split(',')
        print(re_data)
        try:
             table.put_item(
        Item={
            'id': re_data[0],
            'username': re_data[1],
            'lastname': re_data[2]
        })
        except Exception as e:
            raise e
       